use std::cmp::min;
use std::path::PathBuf;
use std::sync::Arc;
use std::thread;
use std::vec;

use clap::Parser;
use image::Rgb;
use image::Rgba;
use image::{GenericImageView, ImageBuffer, Pixel};

const DARK_THRESHOLD: u8 = 64 + 32;
const PRECISION: usize = 3;
const PRECISION_SCALES: usize = 10;
const WINDOW_SIZE: u32 = 17;
const CHROMA_THRESHOLD: u8 = 16;
const FACTOR_OVERSHOOT: f32 = 1.05;
const PARALLELISM: u32 = 16;
fn main() {
    let cli = Cli::parse();
    let img = image::open(cli.input).expect("File not found!");
    let (w, h) = img.dimensions();
    let mut output = ImageBuffer::new(w, h); // create a new buffer for our output
    let mut inter = ImageBuffer::new(w, h); // create a new buffer for our output

    // compute mean and std dev once for whole image
    let pass = img
        .pixels()
        .map(|(_, _, p)| {
            let (mean, std_dev) = mean_and_std_dev(p.0[0] as i32, p.0[1] as i32, p.0[2] as i32);
            Rgb([mean as u8, std_dev as u8, 0])
        })
        .collect::<Vec<Rgb<u8>>>();

    let (chroma_threshold, dark_threshold, fix_r, fix_g, fix_b) = match cli.white {
        Some(raw) => {
            let (x, y) = parse_coordinates(&raw);
            let pxl = img.get_pixel(x, y);
            // let dark_threshold = pxl.to_luma().0[0];
            let (dark_threshold, chroma_threshold) =
                mean_and_std_dev(pxl.0[0].into(), pxl.0[1].into(), pxl.0[2].into());
            let fix_r = dark_threshold - pxl.0[0] as i32;
            let fix_g = dark_threshold - pxl.0[1] as i32;
            let fix_b = dark_threshold - pxl.0[2] as i32;
            (
                chroma_threshold as u8 + 5,
                dark_threshold as u8 + 5,
                fix_r,
                fix_g,
                fix_b,
            )
        }
        None => (CHROMA_THRESHOLD, DARK_THRESHOLD, 0, 0, 0),
    };

    let chunk = h / PARALLELISM;
    let mut handles = vec![];
    let img = Arc::new(img);
    let pass = Arc::new(pass);
    for ii in 0..PARALLELISM {
        let img = img.clone();
        let pass = pass.clone();
        let start = chunk * ii;
        let end = if ii + 1 == PARALLELISM {
            // last job should process rows to the last
            h
        } else {
            chunk * (ii + 1)
        };
        let handle = thread::spawn(move || {
            let mut result_img: Vec<Rgba<u8>> = Vec::with_capacity(chunk as usize * w as usize);
            let mut result_inter: Vec<Rgba<u8>> = Vec::with_capacity(chunk as usize * w as usize);
            for yy in start..end {
                for xx in 0..w {
                    let background_lightness = median_lightness(
                        &pass,
                        w as usize,
                        h as usize,
                        xx as usize,
                        yy as usize,
                        WINDOW_SIZE as usize,
                        chroma_threshold,
                        dark_threshold,
                    );
                    result_inter.push(Rgba([
                        background_lightness,
                        background_lightness,
                        background_lightness,
                        255,
                    ]));
                    let factor = 255.0 * FACTOR_OVERSHOOT / (background_lightness as f32);
                    let pixel = img.get_pixel(xx, yy);
                    // result_img.push(pixel.map(|p| (p as f32 * factor) as u8));
                    result_img.push(Rgba([
                        ((pixel.0[0] as f32 + fix_r as f32) * factor) as u8,
                        ((pixel.0[1] as f32 + fix_g as f32) * factor) as u8,
                        ((pixel.0[2] as f32 + fix_b as f32) * factor) as u8,
                        u8::MAX,
                    ]))
                }
            }
            (result_img, result_inter, start)
        });
        handles.push(handle);
    }
    for handle in handles {
        let (result_img, result_inter, start) =
            handle.join().expect("Could not join thread handle");
        for (idx, pxl) in result_img.iter().enumerate() {
            let x = idx as u32 % w;
            let y = start + (idx as u32 / w);
            output.put_pixel(x, y, *pxl);
        }
        for (idx, pxl) in result_inter.iter().enumerate() {
            let x = idx as u32 % w;
            let y = start + (idx as u32 / w);
            inter.put_pixel(x, y, *pxl);
        }
    }
    output
        .save("output.jpg")
        .expect("Could not save output file");
    inter.save("inter.jpg").expect("Could not save inter file");
    img.save("input.jpeg").expect("Could not save input");
}

fn median_lightness(
    pass: &Arc<Vec<Rgb<u8>>>,
    width: usize,
    height: usize,
    xx: usize,
    yy: usize,
    window: usize,
    chroma_threshold: u8,
    dark_threshold: u8,
) -> u8 {
    let mut histogram: [u32; 256] = [0; 256];
    let mut total = 0;
    for scale in [1, 2, PRECISION_SCALES / 2, PRECISION_SCALES] {
        let scaled_window = window * scale;
        let scaled_precision = PRECISION * scale;
        for y in (yy.saturating_sub(scaled_window)..min(yy + scaled_window, height))
            .step_by(scaled_precision)
        {
            for x in (xx.saturating_sub(scaled_window)..min(xx + scaled_window, width))
                .step_by(scaled_precision)
            {
                let ii = (y) * width + x;
                let pass_pxl = pass[ii].0;
                let rgb_mean = pass_pxl[0];
                let rgb_std_dev = pass_pxl[1];
                let (rgb_mean, rgb_std_dev) = (rgb_mean, rgb_std_dev);
                if rgb_mean > dark_threshold && rgb_std_dev < chroma_threshold {
                    histogram[rgb_mean as usize] += 1;
                    total += 1;
                }
            }
        }
    }
    let mut histosearch: u32 = 0;
    for (ii, val) in histogram.into_iter().enumerate() {
        histosearch += val;
        if histosearch >= total / 2 {
            return ii as u8;
        }
    }
    0
}

fn mean_and_std_dev(r: i32, g: i32, b: i32) -> (i32, i32) {
    let mean = (r as i32 + g as i32 + b as i32) / 3;
    let std_dev = (((r - mean).pow(2) + (g - mean).pow(2) + (b - mean).pow(2)) as f32 / 3.0).sqrt();
    (mean, std_dev as i32)
}

#[derive(Parser)]
struct Cli {
    /// Input file that needs to be processed
    input: PathBuf,

    /// Coordinates of a point on the document that should be white
    #[arg(short, long)]
    white: Option<String>,
}

fn parse_coordinates(raw: &String) -> (u32, u32) {
    let mut split = raw.split('x');
    const ERROR: &str = "No valid coordinates provided";
    let x = split.next().expect(ERROR).parse::<u32>().expect(ERROR);
    let y = split.next().expect(ERROR).parse::<u32>().expect(ERROR);
    (x, y)
}
